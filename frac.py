
import pygame
import sys
import numpy

WIDTH = 100
HEIGHT = 100

if len(sys.argv) == 1:
    JULIA_SQ = 2.0
    JULIA_MIN_X = -JULIA_SQ
    JULIA_MIN_Y = -JULIA_SQ
    JULIA_MAX_X = +JULIA_SQ
    JULIA_MAX_Y = +JULIA_SQ
    MAX_ITERATIONS = 50
    C = complex(-0.75, 0.0)
elif len(sys.argv) == 8:
    JULIA_MIN_X = float(sys.argv[1])
    JULIA_MIN_Y = float(sys.argv[2])
    JULIA_MAX_X = float(sys.argv[3])
    JULIA_MAX_Y = float(sys.argv[4])

    MAX_ITERATIONS = int(sys.argv[5])
    C = complex(float(sys.argv[6]), float(sys.argv[7]))
else:
    sys.exit("usage:    minx, miny, maxx, maxy,  max_iterations, c_real, c_i\n" +
             "defaults: -2.0, -2.0, +2.0, +2.0,      50        , (-0.75, 0)")




print("julia range:", (JULIA_MIN_X, JULIA_MIN_Y), "to", (JULIA_MAX_X, JULIA_MAX_Y))
print("screen size:", (WIDTH, HEIGHT))
print("Max iterations:", MAX_ITERATIONS)
print("Constant", C)

# 3rd dimension: [0] = 1.0 if known break value
#                [1] = num iterations
#                [2] = previous C value real
#                [3] = previous C value imag
#
# if num iterations < this iteration then just use previous value
memo = numpy.zeros([WIDTH, HEIGHT, 4], dtype=numpy.float32)


def set_memo_found(x, y, i, z):
    memo[x][y][0] = numpy.float32(1.0)
    memo[x][y][1] = numpy.float32(i)
    memo[x][y][2] = numpy.float32(z.real)
    memo[x][y][3] = numpy.float32(z.imag)

def get_memo_iterations(x,y):
    return int(memo[x][y][1])

def is_memo_color_found(x, y):
    return memo[x][y][0] == numpy.float32(1.0)


def set_memo_unknown(x, y, i, z):
    memo[x][y][0] = numpy.float32(0.0)
    memo[x][y][1] = numpy.float32(i)
    memo[x][y][2] = numpy.float32(z.real)
    memo[x][y][3] = numpy.float32(z.imag)

def get_memo_unknown(x, y):
    assert memo[x][y][0] == numpy.float32(0.0)
    return complex(memo[x][y][2], memo[x][y][3])

def julia_memo(x: int, y: int, C: complex, max_iter: int) -> float:

    if is_memo_color_found(x,y):
        return get_memo_iterations(x,y)

    if max_iter == 0:
        return 0

    this_iter = max_iter - 1
    if this_iter == 0:
        # work out scaled versions of the screen space coordinates
        scale_x = x / WIDTH
        scale_y = y / HEIGHT

        #transform the screenspace coordinate into the julita set domain space,
        julia_x = JULIA_MIN_X + scale_x * (JULIA_MAX_X - JULIA_MIN_X)
        julia_y = JULIA_MIN_Y + scale_y * (JULIA_MAX_Y - JULIA_MIN_Y)

        z = complex(julia_x, julia_y)
        set_memo_unknown(x, y, this_iter, z)

    z = get_memo_unknown(x, y)
    z = (z ** 2) + C
    if abs(z) > 2:
        set_memo_found(x, y, this_iter, z)
    else:
        set_memo_unknown(x,y, this_iter,z)
    return this_iter

def julia(x: int, y: int, C: complex, max_iter: int) -> float:

    # work out scaled versions of the screen space coordinates
    scale_x = x / WIDTH
    scale_y = y / HEIGHT

    #transform the screenspace coordinate into the julita set domain space,
    julia_x = JULIA_MIN_X + scale_x * (JULIA_MAX_X - JULIA_MIN_X)
    julia_y = JULIA_MIN_Y + scale_y * (JULIA_MAX_Y - JULIA_MIN_Y)

    z = complex(julia_x, julia_y)
    i = 0

    for i in range(max_iter):
        z = (z ** 2) + C
        if abs(z) > 2:
            #print(z, abs(z))
            #print(x,y,"break!", i)
            break


    return i

def choose_color(iterations : int, max_iter : int) -> pygame.Color:
    assert 0 <= iterations <= MAX_ITERATIONS

    if iterations == 0:
        return pygame.Color("Black")
    elif iterations == max_iter - 1:
        return pygame.Color("White")

    col = pygame.Color(0,0,0)
    col.hsla = (((iterations / MAX_ITERATIONS) * 1000.0) % 360.0, 100.0, 50.0, 100.0)
    return col

def main() -> None:
    pygame.init()
    mainClock = pygame.time.Clock()
    windowSurface = pygame.display.set_mode((WIDTH, HEIGHT), 0, 32)
    print(windowSurface.get_rect())

    for max_iter in range(0, MAX_ITERATIONS, 1):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                print("pygame.QUIT!");
                pygame.quit()
                sys.exit()

        #pixArray = pygame.PixelArray(windowSurface)
        pixArray = pygame.surfarray.pixels3d(windowSurface)
        #pixArray = pygame.surfarray.array3d(windowSurface)
        for x in range(WIDTH):
            for y in range(HEIGHT):

                iterations = julia_memo(x, y, C, max_iter)
                #print(iterations)

                col = choose_color(iterations, max_iter)
                pixArray[x][y] = col[0:3]

        #pygame.surfarray.blit_array(windowSurface, pixArray)
        del pixArray

        print(max_iter)
        pygame.display.update()
        pygame.display.flip()


import timeit

start = timeit.default_timer()

main()

stop = timeit.default_timer()

print("time", stop - start)
